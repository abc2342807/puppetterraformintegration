# Configuration Management with Puppet and Terraform 

### Introduction 
This repository contains code and documentation for automating the configuration management of infrastructure resources provisioned using Terraform using Puppet.

### Objective
***
The objective of this assignment is to seamlessly integrate Terraform and Puppet for automated configuration management of infrastructure resources.


### Requirements
***

#### Setup GitLab Repository:
* Create a new GitLab repository named "PuppetTerraformIntegration".
* Initialize the repository with a simple README.md file.
#### Setup Terraform Repository:
* Write Terraform code to provision a simple infrastructure component (e.g., AWS EC2 instance, Azure VM, etc.).
* Store the Terraform configuration files in a directory named "terraform".
#### Configuration Management with Puppet:
* Write Puppet manifests to configure the provisioned infrastructure component.
* Puppet manifests should include tasks such as installing required packages or software, configuring system settings or files, and starting or enabling services.
* Store the Puppet manifests and modules in a directory named "puppet".

#### Integration:
* Create a mechanism to automatically trigger Puppet configuration management after Terraform provisioning.
* This can be achieved using Terraform provisioners to trigger Puppet runs on provisioned instances or utilizing a configuration management tool like Ansible or Shell scripts within Terraform to execute Puppet runs remotely on provisioned instances.

### Running The Integration 
***
Open Terminal and navigate the terraform folder:
```bash
 cd Terraform
```
Run commands:
```bash
terraform init
terraform validate
terraform plan
terraform apply

```

### Installation and Setup
***
#### 1. Prerequisites
Before getting started, ensure that you have the following prerequisites installed on your system:
* Git
* Terraform
* Puppet

#### 2. Setting up GitLab Repository
1. Create a new repository on GitLab named "PuppetTerraformIntegration".
2. Clone the repository to your local machine.
3. Navigate into the cloned repository directory.
4. Initialize the repository with a README.md file.

#### 3. Infrastructure Provisioning with Terraform
1. Write Terraform code to provision a simple infrastructure component, e.g., an AWS EC2 instance.
2. Store the Terraform configuration files in a directory named "terraform".

#### 4. Configuration Management with Puppet
1. Write Puppet manifests to configure the provisioned infrastructure component.
2. Include tasks such as installing required packages or software, configuring system settings or files, and starting or enabling services.
3. Store the Puppet manifests and modules in a directory named "puppet".

#### 5. Integration
Using Terraform Provisioners
1. Add provisioner blocks in your Terraform code to trigger Puppet runs on provisioned instances.
2. Execute Puppet runs remotely on provisioned instances using SSH.


#### 6. Testing
Aws EC2:
***
![Screenshot](Screenshots/EC2 instance.png)

Puppet Agent:
***
![Screenshot](Screenshots/puppet_agent.jpg)

Puppet Server:
***
![Screenshot](Screenshots/puppet_server.jpg)

Trigger to the Puppet Client to run:
***
![Screenshot](Screenshots/nginx.png)
***
### Contributor
Rasika Sherkar - WDGET2024010










