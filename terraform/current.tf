terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
    region = "us-east-1"
}

resource "aws_instance" "Puppetmaster" {
    ami = "ami-0f403e3180720dd7e"
    instance_type = "t2.micro"
    key_name = "puppet"
    tags = {
      Name = "Puppetmaster"
    }

    provisioner "remote-exec" {
       inline=[
        "sudo /opt/puppetlabs/puppet/bin/puppet agent -t --server=puppetmaster.local",
       ]
       connection {
            type        = "ssh"
            user        = "ubuntu"
            private_key = file("C:/Users/DELL/OneDrive - Sinhgad Academy of Engineering/Documents/Project terraform/puppet_key.pem")
            host        = aws_instance.puppetmaster.public_ip
            agent       = true
        }
    }
}
